/***************************************************************************
 *  Project                Wiz2MQTT
 *
 * Copyright (C) 2022 , Dmitriy Korovkin
 *
 * This software is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the LICENSE file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 * @file smartbulb
 *
 ***************************************************************************/

#include <smartbulb.hpp>
#include <sstream>

SmartBulb::SmartBulb(const string name, const string ipAddr,
    const string macAddr) : m_name(name)
    {
    setDeviceIP(ipAddr);
    m_devMac = stoull(macAddr, NULL, 16);
    }

SmartBulb::~SmartBulb()
    {
    }

ostream& operator<<(ostream& os, const SmartBulb& bulb)
    {
    os << bulb.m_name << " "
       << bulb.getDeviceIp() << " "
       << hex << bulb.getDeviceMac();
    return os;
    }

const string SmartBulb::toString(void) const
    {
    ostringstream os;
    os << m_name << " "
       << getDeviceIp() << " "
       << hex << getDeviceMac();
    return os.str();
    }
