/***************************************************************************
 *  Project                Wiz2MQTT
 *
 * Copyright (C) 2022 , Dmitriy Korovkin
 *
 * This software is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the LICENSE file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 * @file wiz2mqtt
 *
 ***************************************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <log.h>
#include <wiz2mqtt.hpp>
#include <config.hpp>
#include <jansson.h>
#include <unistd.h>
#include <mqtt/client.h>
#include <mqtt/ssl_options.h>
#include <getopt.h>

#include <mcheck.h>

#undef DEBUG

#ifdef DEBUG
#define CFG_DIR_PREFIX "../"
#else
#define CFG_DIR_PREFIX "/usr/local/etc/wiz2mqtt"
#endif

const char* c_cfgFileName = CFG_DIR_PREFIX
    "/wiz2mqtt.json";

using namespace std;

const int discDelay = 3; // delays between discovery retries
const int discRetry = 3; // number of discovery retries
const int retryDelay = 10; // delays between MQTT broker reconnect attempts

/* Result code of the MQTT commands */
const int W2MQTT_OK       = 0;
const int W2MQTT_EXIT     = 1;
const int W2MQTT_DISCOVER = 2;

/* Protocols for the MQTT broker URI */
const string mqttProtoSSL { "ssl://" };
const string mqttProtoTCP { "tcp://" };

bool debug = false; // print additional debug information
bool forcePersist = false; // force the following option to be true
bool persistConnection = true; // reconnect to MQTT broker on error

/*
 * Command handling functions
 */

string bulbReboot(SmartBulb& bulb, wizArg arg)
    {
    return bulb.reboot();
    }

string bulbBrightness(SmartBulb& bulb, wizArg arg)
    {
    ushort l_arg;
    if (arg.arg > maxBrightness)
        {
        l_arg = maxBrightness;
        }
    else if (arg.arg < minBrightness)
        {
        l_arg = minBrightness;
        }
    else l_arg = ushort(arg.arg);

    return bulb.setBrightness(l_arg);
    }

string bulbToggle(SmartBulb& bulb, wizArg arg)
    {
    return bulb.toggleLight((arg.arg != 0));
    }

string bulbScene(SmartBulb& bulb, wizArg arg)
    {
    return bulb.setScene(ushort(arg.arg));
    }

string bulbColorTemp(SmartBulb& bulb, wizArg arg)
    {
    if (arg.arg > maxColorTemp)
        {
        arg.arg = maxColorTemp;
        }
    else if (arg.arg < minColorTemp)
        {
        arg.arg = minColorTemp;
        }
    return bulb.setColorTemp(ushort(arg.arg));
    }

string bulbRGB(SmartBulb& bulb, wizArg arg)
    {
    return bulb.setRGBColor(arg.rgb.rgb[wizRED],
        arg.rgb.rgb[wizGREEN], arg.rgb.rgb[wizBLUE]);
    }

/*
 * Table of the command handlers
 */
cmdHandler bulbCmd[] =
    {
        {
        "reboot",
        bulbReboot
        },
        {
        "brightness",
        bulbBrightness
        },
        {
        "toggle",
        bulbToggle
        },
        {
        "scene",
        bulbScene
        },
        {
        "setrgb",
        bulbRGB
        },
        {
        "colortemp",
        bulbColorTemp
        },
        { /* last element of the array */
        "",
        NULL
        }
    };

/*
 * Parse topic and get bulb name
 *
 * Returns bulb name or the whole topic if it is not correct.
 */
const string getBulb(string topic)
    {
    size_t pos = topic.rfind('/');
    if (pos != string::npos)
        {
        return topic.substr(pos + 1);
        }
    else
        {
        return topic;
        }
    }

/*
 * Parses the message and obtains the command and arguments
 *
 * Returns 0 on success -1 otherwise
 */
int getCommand(string msg, string& command, wizArg& args)
    {
    int result = -1;
    json_error_t error;
    wizArg l_args;
    string l_cmd;
    json_t* root = json_loads(msg.c_str(), 0, &error);

    try
        {
        if(!json_is_object(root))
            {
            throw string("Message not a JSON");
            }

        /* extract command first */
        json_t* l_cmd_object = json_object_get(root, "command");
        if (!json_is_string(l_cmd_object))
            {
            throw string("JSON message command parsing error");
            }
        l_cmd = json_string_value(l_cmd_object);

        /*
         * Extract arguments
         * Arguments can be presented either as an integer above 0
         * or as an array of three integers above 0. In the second case
         * the final argument has to becalculated out of the array
         */
        json_t* l_cmd_arg = json_object_get(root, "arg");
        if (json_is_integer(l_cmd_arg))
            {
            l_args.arg = json_integer_value(l_cmd_arg);
            }
        else if (json_is_array(l_cmd_arg))
            {
            int i;
            int nArgs = json_array_size(l_cmd_arg);

            if (nArgs != 3)
                {
                throw string("JSON message argument parsing error");
                }
            for (i = 0; i < nArgs; i++)
                {
                json_t* l_item_object = json_array_get(l_cmd_arg, i);
                if (!json_is_integer(l_item_object))
                    {
                    throw string("JSON message argument parsing error");
                    }
                ushort l_item = (ushort)json_integer_value(l_item_object);
                /*
                 * Color value for wiz lamps can't be <10, although OpenHAB
                 * does not allow setting a limit for the value. We set it here.
                 */
                l_args.rgb.rgb[i] = (l_item < minBrightness)?
                        minBrightness: l_item;
                }
            }
        else
            {
            throw string("JSON message argument parsing error");
            }

        command = l_cmd;
        args = l_args;
        result = 0;
        }
    catch (string msg)
        {
        LOG_E(msg.c_str());
        }
    json_decref(root);
    return result;
    }

/*
 * Discovers bulbs and fils in configuration bulb list
 *
 */
void discover(Config& config, bool clearList)
    {
    int i;
    LOG_I("Discovering lamps");
    if (clearList)
        {
        config.clearBulbIp();
        }
    for (i = 0; i < discRetry; i++)
        {
        if (i != 0) sleep(discDelay);
        if (config.discover() == 0) break;
        }

    for (const SmartBulb& l_bulb : config.m_bulbList)
        {
        LOG_I(l_bulb.toString().c_str());
        }

    if (!config.m_unregBulbs.empty())
        {
        LOG_I("Unconfigured lamps");
        for (const SmartBulb& l_bulb : config.m_unregBulbs)
            {
            LOG_I(l_bulb.toString().c_str());
            }
        }
    }

/*
 * Function parses reply status in the reply string
 *
 * Reply is represented in JSON format as:
 * { "bulb_response": { "success": false, "message": "\<error message\>" } }
 *
 * Returns 1 if success is true, 0 if success is false, -1 on parsing error
 */
static int parseReplyStatus(string& reply)
    {
    int result = -1;
    json_error_t error;
    json_t* root = json_loads(reply.c_str(), 0, &error);
    try
        {
        if(!json_is_object(root))
            {
            throw string("Message not a JSON:" + reply);
            }
        json_t* l_response_object = json_object_get(root, "bulb_response");
        if(!json_is_object(l_response_object))
            {
            throw string("Error parsing: " + reply);
            }
        json_t* l_status_object = json_object_get(l_response_object,
            "success");
        if (!json_is_boolean(l_status_object))
            {
            throw string("Error status parsing: " + reply);
            }
        result = json_boolean_value(l_status_object);
        }
    catch (string msg)
        {
        result = -1;
        LOG_E(msg.c_str());
        }
    json_decref(root);
    return result;
    }

/*
 * Function parses message and invokes the message handler with the
 * bulb from the vector which name matches the name in topic. On
 * success fills in the reply string.
 *
 * Topic format: bulbs/<bulb name>
 *
 * Returns: 0 on success, -1 on error
 */
int handleMessage(shared_ptr<const mqtt::message> msg,
    string& bulbName, string& reply,
    Config& config)
    {
    string cmd;
    wizArg args;
    string commandTopic {COMMAND_TOPIC};

    string bulb = getBulb(msg->get_topic());

    if (getCommand(msg->to_string(), cmd, args) == 0)
        {
        LOG_D("%s: %s(%u)", bulb.c_str(), cmd.c_str(), args.arg);
        if (bulb == commandTopic) /* general bulb commands */
            {
            if (cmd == "discover")
                {
                discover(config, true);
                return W2MQTT_DISCOVER;
                }
            if (cmd == "exit")
                {
                return W2MQTT_EXIT;
                }
            }
        else /* individual bulb commands */
            {
            int i;
            bulbCommandFunc bulbFunc = NULL;

            for (i = 0; !bulbCmd[i].command.empty(); i++)
                {
                if (cmd == bulbCmd[i].command)
                    {
                    bulbFunc = bulbCmd[i].handler;
                    break;
                    }
                }
            if (bulbFunc != NULL)
                {
                int l_ret = 1;
                bool l_error = false;

                /*
                 * Run a command for the bulb(s) with the name
                 * in bulb. If there are several bulbs with the same
                 * name (a group), run command for all of them. If one
                 * of the bulbs or more have no IP address configured,
                 * does not reply or replies with an error, keep this
                 * reply and do not overwrite it. If several bulbs
                 * err, then keep the first reply with an error.
                 */
                for (SmartBulb& l_bulb : config.m_bulbList)
                    {
                    if (l_bulb.getDeviceName() == bulb)
                        {
                        string result { NO_IP_ADDRESS };
                        if (!l_bulb.getDeviceIp().empty())
                            {
                            result = bulbFunc(l_bulb, args);
                            }
                        else
                            {
                            l_ret = 0;
                            }
                        if (!l_error)
                            {
                            bulbName = bulb;
                            reply = result;
                            l_ret = parseReplyStatus(result);
                            }
                        if (l_ret == 0 || l_ret == -1)
                            {
                            l_error = true;
                            }
                        LOG_D("%s", result.c_str());
                        }
                    }
                }
            else
                {
                bulbName = bulb;
                reply = NO_COMMAND_ERROR;
                LOG_E("Command not found for: %s: %s\n",
                    msg->get_topic().c_str(),
                    msg->to_string().c_str());
                }
            }
        }
    else
        {
        bulbName = bulb;
        reply = MESSAGE_PARSING_ERROR;
        LOG_E("Parse error for: %s: %s\n",
            msg->get_topic().c_str(),
            msg->to_string().c_str());
        }
    return W2MQTT_OK;
    }

/*
 * Function sends status message for each bulb to MQTT broker
 */
static void sendBulbsStatus(mqtt::client& cli, Config& config)
    {
    /* if client is not connected, nothing we can do */
    if (!cli.is_connected())
        return;

    /* Send status message to MQTT broker */
    for (SmartBulb& l_bulb : config.m_bulbList)
        {
        string topic (MQTT_TOPIC_PREFIX);
        topic += l_bulb.getDeviceName();
        topic += MQTT_STATUS_SUFFIX;
        string reply;
        if (l_bulb.getDeviceIp().empty())
            {
            reply = NO_IP_ADDRESS;
            }
        else
            {
            reply = OK_RESULT;
            }
        mqtt::message mqttMsg(topic,
            reply.c_str(), reply.size(), QOS_1, false);
        LOG_D("MQTT publish topic: %s, message %s",
            topic.c_str(), reply.c_str());
        cli.publish(mqttMsg);
        }
    }

/*
 * Function sends status message for the whole list of the bulbs.
 * It reports status true if all the bulbs have IP address and
 * false if at least one bulb has IP address empty
 */
static void sendBulbListStatus(mqtt::client& cli, Config& config)
    {
    string topic (MQTT_TOPIC_PREFIX);
    topic += DISCOVERY_RESULT_TOPIC;
    topic += MQTT_STATUS_SUFFIX;
    string reply;

    /* if client is not connected, nothing we can do */
    if (!cli.is_connected())
        return;

    if (config.bulbsHaveEmptyIp())
        {
        reply = NO_IP_ADDRESS;
        }
    else
        {
        reply = OK_RESULT;
        }

    /* Send status message to MQTT broker */
    mqtt::message mqttMsg(topic,
        reply.c_str(), reply.size(), QOS_1, false);
    LOG_D("MQTT publish topic: %s, message %s",
        topic.c_str(), reply.c_str());
    cli.publish(mqttMsg);
    }

/*
 * Log MQTT exception information
 */
void logMqttException(const mqtt::exception& exc, int err)
    {
        ostringstream os;
        os << "Error: " << exc.what() << " ["
            << exc.get_reason_code() << "]";
        LOG_E("%s", os.str().c_str());
        LOG_E(strerror(err));
    }

/*
 * Main function of the program
 */
int main(int argc, char *argv[])
    {
    int opt;

    while ((opt = getopt (argc, argv, "dp")) != -1)
        {
        switch (opt)
            {
            case 'd':
                debug = true;
                break;
            case 'p':
                forcePersist = true;
                break;
            default:
                cout << "Usage " << argv[0] << " [-dp]" << endl;
                cout << "where:" << endl;
                cout << "  -d - enable debugging" << endl;
                cout << "  -p - reconnect to MQTT broker on "
                    "connection error" << endl;
                exit(0);
            }
        }
    if (debug)
        {
        mtrace();
        L::setLogLevel(L::d);
        persistConnection = false;
        }
    else
        {
        persistConnection = true;
        }

    /*
     * Unless forced the persist connection is off when
     * debug is enabled and on otherwise
     */
    if (forcePersist)
        {
        persistConnection = true;
        }
    try
        {
        LOG_I("Reading config file");
        string filename(c_cfgFileName);
        Config config(filename);
        vector<string> topics;
        vector<int> qos;

        config.buildTopics(topics, qos);
        mqtt::client cli(config.m_serverURI, config.m_clientName);
        auto connOpts = mqtt::connect_options_builder()
            .keep_alive_interval(chrono::seconds(10))
            .automatic_reconnect(chrono::seconds(2), chrono::seconds(120))
            .clean_session(true)
            .finalize();

        if (config.m_serverURI.compare(0, mqttProtoSSL.size(), mqttProtoSSL)
            == 0)
            {
            auto sslOpt = mqtt::ssl_options_builder()
                .trust_store(config.m_trustStore)
                .error_handler([](const string& msg) {
                        LOG_E("SSL Error: %s", msg.c_str());
                        })
                .finalize();
            connOpts.set_ssl(sslOpt);
            LOG_D("Secure MQTT connection");
            }
        else
            {
            LOG_D("Insecure MQTT connection");
            }
        if (!debug)
            {
            daemon(0, 0);
            L::setDaemon(true);
            }
        do
            {
            try
                {
                mqtt::connect_response rsp = cli.connect(connOpts);
                }
            catch (const mqtt::exception& exc)
                {
                if (persistConnection)
                    {
                    logMqttException(exc, errno);
                    this_thread::sleep_for(chrono::seconds(retryDelay));
                    }
                else
                    {
                    throw exc;
                    }
                }
            } while (persistConnection && !cli.is_connected());

        /*
         * If we start with a clean session, we always
         * resubscribe for the topics
         */
        cli.subscribe(topics, qos);
        discover(config, true);
        sendBulbsStatus(cli, config);
        sendBulbListStatus(cli, config);

        /* Receive and process MQTT messages */
        while (true)
            {
            mqtt::const_message_ptr msg;
            bool is_msg = cli.try_consume_message_for(&msg, std::chrono::seconds(10));
            if (is_msg && msg!= NULL)
                {
                string reply;
                string bulb;
                int result = handleMessage(msg, bulb, reply, config);
                if (result == W2MQTT_EXIT)
                    {
                    break;
                    }
                else if (result == W2MQTT_DISCOVER)
                    {
                    sendBulbsStatus(cli, config);
                    sendBulbListStatus(cli, config);
                    string topic (MQTT_TOPIC_PREFIX);
                    topic += COMMAND_TOPIC;
                    topic += MQTT_STATUS_SUFFIX;
                    mqtt::message mqttMsg(topic,
                        OK_RESULT.c_str(), OK_RESULT.size(),
                        QOS_1, false);
                    LOG_D("MQTT publish topic: %s, message %s",
                        topic.c_str(), OK_RESULT.c_str());
                    cli.publish(mqttMsg);
                    }
                else if (!reply.empty() && !bulb.empty())
                    {
                    string topic (MQTT_TOPIC_PREFIX);
                    topic += bulb;
                    topic += MQTT_STATUS_SUFFIX;
                    mqtt::message mqttMsg(topic,
                        reply.c_str(), reply.size(), QOS_1, false);
                    LOG_D("MQTT publish topic: %s, message %s",
                        topic.c_str(), reply.c_str());
                    cli.publish(mqttMsg);
                    }
                }
            else if (!cli.is_connected())
                {
                bool mqttSessionPresent;

                LOG_I("Reconnecting to MQTT broker");
                while (!cli.is_connected())
                    {
                    this_thread::sleep_for(chrono::seconds(retryDelay));
                    try
                        {
                        mqtt::connect_response rsp = cli.reconnect();
                        mqttSessionPresent = rsp.is_session_present();
                        }
                    catch (const mqtt::exception& exc)
                        {
                        logMqttException(exc, errno);
                        }
                    }
                LOG_D("Connected to MQTT broker");

                /*
                 * If we reconnect to the broker there is a chance that
                 * the session may exist
                 */
                if (!mqttSessionPresent)
                    {
                    LOG_I("Subscribing to MQTT topics");
                    cli.subscribe(topics, qos);
                    }
                }
            }
        LOG_I("Exiting");
        cli.disconnect();
        }
    /* Catch general errors */
    catch (string msg)
        {
        LOG_E("%s", msg.c_str());
        }
    /* Catch MQTT related errors */
    catch (const mqtt::exception& exc)
        {
        logMqttException(exc, errno);
        }
    return 0;
    }
