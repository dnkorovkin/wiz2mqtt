/***************************************************************************
 *  Project                Wiz2MQTT
 *
 * Copyright (C) 2022 , Dmitriy Korovkin
 *
 * This software is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the LICENSE file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 * @file config
 *
 ***************************************************************************/
#include <config.hpp>

/*
 * Default configuration settings
 */

const char* defClientName = "wiz2mqtt";
const char* defTrustStore = "/etc/wiz2mqtt/ca.crt";

Config::Config(string cfgFileName) :
    m_bulbList(),
    m_unregBulbs(),
    m_serverURI(),
    m_clientName(defClientName),
    m_trustStore(defTrustStore),
    m_bcastIpAddress()
    {
    parseConfig(cfgFileName);
    }

Config::~Config()
    {
    }

/*
 * Parse broadcast IP address and fills
 * m_bcastIpAddress.
 *
 * Throws a string exception on error
 */
void Config::parseBcastIpAddr(json_t *root)
    {
    json_t *bcastIp = json_object_get(root, "bcastIP");
    if (!json_is_string(bcastIp))
        {
        json_decref(root);
        throw string("Error reading broadcast IP address");
        }
    m_bcastIpAddress = json_string_value(bcastIp);
    }

/*
 * Parse MQTT parameters and fills in the member variables
 *
 * Throws a string exception on error
 */
void Config::parseMqttConfig(json_t *root)
    {
    json_t *mqtt = json_object_get(root, "mqtt");
    if (!json_is_object(mqtt))
        {
        json_decref(root);
        throw string("Error reading MQTT settings");
        }
    json_t *serverURI = json_object_get(mqtt, "serverURI");
    if (!json_is_string(serverURI))
        {
        json_decref(root);
        throw string("Error reading MQTT server URI");
        }
    m_serverURI = json_string_value(serverURI);

    /*
     * We have default parameters. If there is no such ones in
     * the configuration file, stay with the defaults.
     */
    json_t *trustStore = json_object_get(mqtt, "trustStore");
    if (json_is_string(trustStore))
        {
        m_trustStore = json_string_value(trustStore);
        }
    json_t *clientName = json_object_get(mqtt, "clientName");
    if (json_is_string(clientName))
        {
        m_clientName = json_string_value(clientName);
        }
    }

/*
 * Parse the bulbs configuration file and fills
 * the vector of SmartBulb class variables
 *
 * Throws a string exception on error
 */
void Config::parseBulbsConfig(json_t *root)
    {
    unsigned int i;
    vector<SmartBulb> l_bulbs = {};
    json_t *bulbs = json_object_get(root, "bulbs");
    if (!json_is_array(bulbs))
        {
        json_decref(root);
        throw string("Error reading bulbs array");
        }
    for (i = 0; i < json_array_size(bulbs); i++)
        {
        json_t *data;
        json_t *devData;
        json_t *mac;
        json_t *name;
        string devName;
        string devMac = "0x";

        data = json_array_get(bulbs, i);
        devData = json_object_get(data, "bulb");
        if(!json_is_object(data) || !json_is_object(devData))
            {
            json_decref(root);
            throw string("Not an object at " + to_string(i));
            }
        mac = json_object_get(devData, "mac");
        name = json_object_get(devData, "name");
        if(!json_is_string(mac) || !json_is_string(name))
            {
            json_decref(root);
            throw string("Object read error");
            }
        devMac += json_string_value(mac);
        devName = json_string_value(name);
        SmartBulb l_bulb(devName, "", devMac);
        l_bulbs.push_back(l_bulb);
        }
    m_bulbList = l_bulbs;
    }

/*
 * Parses JSON configuration file and fills in the class member variables
 *
 * Called functions throw string exception on error
 */
void Config::parseConfig(string filename)
    {
    json_error_t error;
    json_t *root = json_load_file(filename.c_str(), 0, &error);
    if(!json_is_object(root))
        {
        json_decref(root);
        throw string("Wrong data: " + string(error.text));
        }

    parseBcastIpAddr(root);
    parseMqttConfig(root);
    parseBulbsConfig(root);
    json_decref(root);
    }

/*
 * Function sends a discovery packet to broadcast address, [m_bcastIpAddress]
 * parses the reply and updates the bulb list [bulbList] with IP addresses
 * found
 *
 * Returns 0 if all the bulbs have IP addresses and -1 otherwise.
 */
int Config::discover(void)
    {
    unsigned int i;
    SmartBulb bulb;
    json_error_t error;

    string ret = bulb.discover(m_bcastIpAddress);
    if (ret.empty())
        {
        return -1;
        }

    json_t *root = json_loads(ret.c_str(), 0, &error);

    if (!root || !json_is_array(root))
        {
        json_decref(root);
        throw string("Wrong data");
        }
    for (i = 0; i < json_array_size(root); i++)
        {
        json_t *data;
        json_t *devData;
        json_t *mac;
        json_t *ipaddr;
        string devIp;
        string szdevMac = "0x";
        uint64_t devMac = 0ul;
        bool bulbInList;

        data = json_array_get(root, i);
        devData = json_object_get(data, "bulb_response");
        if(!json_is_object(data) || !json_is_object(devData))
            {
            json_decref(root);
            throw string("Not an object at " + to_string(i));
            }
        mac = json_object_get(devData, "devMac");
        ipaddr = json_object_get(devData, "ip");
        if(!json_is_string(mac) || !json_is_string(ipaddr))
            {
            json_decref(root);
            throw string("Object read error");
            }
        szdevMac += json_string_value(mac);
        devMac = stoull(szdevMac, NULL, 16);
        devIp = json_string_value(ipaddr);

        bulbInList = false;
        for (SmartBulb& l_bulb : m_bulbList)
            {
            if (l_bulb.getDeviceMac() == devMac)
                {
                bulbInList = true;
                l_bulb.setDeviceIP(devIp);
                }
            }

        /*
         * Bulb is not configured let's check if it exists in the
         * list of unconfigured bulbs
         */
        if (!bulbInList)
            {
            for (SmartBulb& l_bulb : m_unregBulbs)
                {
                if (l_bulb.getDeviceMac() == devMac)
                    {
                    bulbInList = true;
                    }
                }
            /* If the bulb is not in the unregistered list, add it */
            if (!bulbInList)
                {
                SmartBulb l_bulb("Unregistered", devIp, szdevMac);
                m_unregBulbs.push_back(l_bulb);
                }
            }
        }
    json_decref(root);
    for (SmartBulb& l_bulb : m_bulbList)
        {
        if (l_bulb.getDeviceIp().empty())
            {
            return -1;
            }
        }
    return 0;
    }

/*
 * Clear IP addresses of the bulbs in the configuration
 */
void Config::clearBulbIp(void)
    {
    for (SmartBulb& l_bulb : m_bulbList)
        {
        l_bulb.setDeviceIP("");
        }

    /* Also clean the list of unconfigured bulbs */
    m_unregBulbs.clear();
    }

void Config::buildTopics(vector<string>& topics, vector<int>& qos)
    {
    string mqttTopicPrefix {MQTT_TOPIC_PREFIX};
    string commandTopic {COMMAND_TOPIC};
    topics.clear();
    qos.clear();
    for (SmartBulb& l_bulb : m_bulbList)
        {
        string topic = mqttTopicPrefix + l_bulb.getDeviceName();
        topics.push_back(topic);
        qos.push_back(QOS_1);
        }

    /* topic for general bulbs command */
    topics.push_back(mqttTopicPrefix + commandTopic);
    qos.push_back(QOS_1);
    }

/*
 * Check if there are bulbs with empty IP address in the list
 *
 * Returns TRUE if any bulb in the list has empty IP address
 * and false otherwise
 */
bool Config::bulbsHaveEmptyIp(void)
    {
    for (SmartBulb& l_bulb : m_bulbList)
        {
        if (l_bulb.getDeviceIp().empty())
            {
            return true;
            }
        }
    return false;
    }
