/***************************************************************************
 *  Project                Wiz2MQTT
 *
 * Copyright (C) 2022 , Dmitriy Korovkin
 *
 * This software is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the LICENSE file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 * @file wizutil
 *
 ***************************************************************************/
#include <log.h>
#include <config.hpp>
#include <jansson.h>
#include <unistd.h>
#include <getopt.h>
#include <inttypes.h>

#undef DEBUG

#ifdef DEBUG
#define CFG_DIR_PREFIX "../"
#else
#define CFG_DIR_PREFIX "/usr/local/etc/wiz2mqtt"
#endif

const char* c_cfgFileName = CFG_DIR_PREFIX
    "/wiz2mqtt.json";

using namespace std;

const int discDelay = 3; // delays between disCovery retries
const int discRetry = 3; // number ofdiscovery retries

/*
 * Discovers bulbs and fils in configuration bulb list
 *
 */
void discover(Config& config, bool clearList)
    {
    int i;
    LOG_I("Discovering lamps");
    if (clearList)
        {
        config.clearBulbIp();
        }
    for (i = 0; i < discRetry; i++)
        {
        if (i != 0) sleep(discDelay);
        if (config.discover() == 0) break;
        }

    for (const SmartBulb& l_bulb : config.m_bulbList)
        {
        LOG_I(l_bulb.toString().c_str());
        }

    if (!config.m_unregBulbs.empty())
        {
        LOG_I("Unconfigured lamps");
        for (const SmartBulb& l_bulb : config.m_unregBulbs)
            {
            LOG_I("{");
            LOG_I("\t\"bulb\": {");
            LOG_I("\t\t\"mac\": \"%" PRIx64 "\",", l_bulb.getDeviceMac());
            LOG_I("\t\t\"name\": \"Unconfigured lamp\"");
            LOG_I("\t}");
            LOG_I("},");
            }
        }
    }


/*
 * Main function of the program
 */
int main(int argc, char *argv[])
    {
    int opt;
    string filename(c_cfgFileName);

    while ((opt = getopt (argc, argv, "c:")) != -1)
        {
        switch (opt)
            {
            case 'c':
                filename = optarg;
                break;
            default:
                cout << "Usage " << argv[0] << " [-c <config file>]" << endl;
                cout << "where:" << endl;
                cout << "  -c - custom config file" << endl;
                exit(0);
                break;
            }
        }
    try
        {
        LOG_I("Reading config file");
        Config config(filename);

        discover(config, true);
        }
    /* Catch general errors */
    catch (string msg)
        {
        LOG_E("%s", msg.c_str());
        }

    return 0;
    }
