# Wiz2MQTT

Wiz2MQTT is a gateway between Wiz [Wiz](https://www.wizconnected.com/en-us) 
bulbs and MQTT broker.

Wiz2MQTT provides the interface between Wiz lamps and MQTT broker and allows
the bulbs get controlled by a home automation tool that provides a
communication with MQTT server. Examples are:
[OpenHAB](https://www.openhab.org)
or
[Home Assistant](https://www.home-assistant.io)

In this configuration Wiz2MQTT subscribes on the command topics described
below and receives messages from the home automation tool. The home automation
tool in turn generates the messages and subscribes on the status topics
described below, receives and parses status messages that come from
Wiz2MQTT.

## Acknowledgements

Project uses components from wizlightcpp project, found
[here:](https://github.com/srisham/wizlightcpp.git)

## Interface

In order to command Wiz bulbs send JSON encoded MQTT messages to the MQTT
broker.
Message topic for each bulb is "bulbs/\<bulb name\>", where \<bulb name\> is the
name specified in the wiz2mqtt.json configuration file for individual bulb or
bulb groups.

### Toggle ON/OFF

OFF message: { "command": "toggle", "arg": 0 }  
ON message:  { "command": "toggle", "arg": 1 }  

### Set Bulb Brightness

Message: { "command": "brightness", "arg": \<brightness value\> }  
\<brightness value\> may take values from 10 to 100.

### Set Bulb Color Temperature

Message: { "command": "colortemp", "arg": \<color temperature K\> }  
\<color temperature K\> may take values from 1000 to 8000.

### Set Bulb Scene

Message: { "command": "scene", "arg": \<scene id\> }  

\<scene id\> may take values from 1 to 32:  
1 -  Ocean  
2 -  Romance  
3 -  Sunset  
4 -  Party  
5 -  ireplace  
6 -  Cozy  
7 -  Forest  
8 -  Pastel Colors  
9 -  Wake up  
10 - Bedtime  
11 - Warm White  
12 - Daylight  
13 - Cool white  
14 - Night light  
15 - Focus  
16 - Relax  
17 - True colors  
18 - TV time  
19 - Plantgrowth  
20 - Spring  
21 - Summer  
22 - Fall  
23 - Deepdive  
24 - Jungle  
25 - Mojito  
26 - Club  
27 - Christmas  
28 - Halloween  
29 - Candlelight  
30 - Golden white  
31 - Pulse  
32 - Steampunk  

### Bulb Reboot

Message: { "command": "reboot", "arg": 0 }  

### Set color

Message: { "command": "setrgb", "arg": [r, g, b] }  
where:  
r - red value [10 .. 255]  
g - green value [10 .. 255]  
b - blue value [10 .. 255]  

### Replies

Reply comes from Wiz2MQTT to MQTT broker. It uses the topic  
"bulbs/\<bulb name\>/status"

If operation was successful, the message has the following format:  
{ "bulb_response": { "success": true } }

If the operation failed, the reply has the following format:  
{ "bulb_response": { "success": false, "message": "\<error message\>" } }

### Bulb discovery

Bulb discovery sends a broadcast request and collects replies from the bulbs in 
the network. The purpose of the discovery - obtain the bulbs IP addresses.

Discover command is sent to the topic "bulbs/proc_cmd" and has the following
format:  
{ "command": "discover", "arg": 0 }

When the discovery complete Wiz2MQTT replies on the topic for each bulb as
"bulbs/\<bulb name\>/status" "with the message  
{ "bulb_response": { "success": true } }" for each bulb of which Wiz2MQTT 
obtained it's IP address or  
{ "bulb_response": { "success": false, "message": "No IP address" } }"
for each bulb of which Wiz2MQTT could not obtain one.

Wiz2MQTT also replies on the topic "bulbs/proc_cmd/status" with the message  
{ "bulb_response": { "success": true } }  
indicating the end of the discovery process. It replies on the topic
"bulbs/discovered all/status" with the message  
{ "bulb_response": { "success": true } } if all the lamps were discovered 
and  
{ "bulb_response": { "success": false, "message": "No IP address" } } if
any of the lamps has not responded.

### Exit

Exit command is used to stop Wiz2MQTT.

Exit command is sent to the topic "bulbs/proc_cmd" and has the following format:
{ "command": "exit", "arg": 0 }

Wiz2MQTT exits immediately without sending any reply.

## Building

### Required libraries

To build you need the following libraries:
  * [Paho MQTT C](https://github.com/eclipse/paho.mqtt.c.git)
  * [Paho MQTT C++](https://github.com/eclipse/paho.mqtt.cpp.git)
  * [Jansson](https://github.com/akheron/jansson)

### Building steps

    mkdir build  
    cd build  
    cmake ..  
    make  

To build Debian package run:

    make package

## Running

    wiz2mqtt [-d]  
    where:  
        -d - enable debug output.  
        -p - reconnect to MQTT broker on connection error.  

## Unconfigured bulbs

When adding a new lightbulb to the sustem the **wizutil** does the bulb
discovery and prints both configured, ie listed in the wiz2mqtt.json config
file bulbs as well as not configured ones. Command format:

    wizutil [-c <config file>]  
    where:  
        -c - provide a custom config file.
