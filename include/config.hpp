/***************************************************************************
 *  Project                Wiz2MQTT
 *
 * Copyright (C) 2022 , Dmitriy Korovkin
 *
 * This software is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the LICENSE file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 * @file config
 * 
 ***************************************************************************/
#pragma once

#include <string>
#include <vector>
#include <jansson.h>
#include <smartbulb.hpp>

/* General constants */

#define MQTT_TOPIC_PREFIX "bulbs/"
#define MQTT_STATUS_SUFFIX "/status"
#define COMMAND_TOPIC "proc_cmd"
#define DISCOVERY_RESULT_TOPIC "discovered all"

constexpr int QOS_0 = 0;
constexpr int QOS_1 = 1;

constexpr int minBrightness = 10;
constexpr int maxBrightness = 100;

constexpr int minColorTemp = 1000;
constexpr int maxColorTemp = 8000;

using namespace std;

class Config
    {
public:

    vector<SmartBulb> m_bulbList;
    vector<SmartBulb> m_unregBulbs;
    string m_serverURI;
    string m_clientName;
    string m_trustStore;

    Config(string cfgFileName);
    ~Config();
    const string& getBcastIp(void)
        {
        return m_bcastIpAddress;
        }
    int discover(void);
    bool bulbsHaveEmptyIp(void);
    void clearBulbIp(void);
    void buildTopics(vector<string>& topics, vector<int>& qos);
    
protected:

    string m_bcastIpAddress; // broadcast IP address for discovery

private:

    void parseConfig(string filename);
    void parseBcastIpAddr(json_t *root);
    void parseBulbsConfig(json_t *root);
    void parseMqttConfig(json_t *root);
    };

