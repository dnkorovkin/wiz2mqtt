/***************************************************************************
 *  Project                Wiz2MQTT
 *
 * Copyright (C) 2022 , Dmitriy Korovkin
 *
 * This software is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the LICENSE file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 * @file wiz2mqtt
 *
 ***************************************************************************/
#pragma once

#include <string>
#include <smartbulb.hpp>
#include <stdint.h>

enum
    {
    wizRED = 0,
    wizGREEN,
    wizBLUE
    };

typedef struct
    {
    ushort rgb[3];
    }
    wizColor;

typedef union
    {
    uint32_t arg;
    wizColor rgb;
    }
    wizArg;

typedef string (*bulbCommandFunc)(SmartBulb& bulb, wizArg arg);

typedef struct
    {
    string command;
    bulbCommandFunc handler;
    }
    cmdHandler;
