/***************************************************************************
 *  Project                Wiz2MQTT
 *
 * Copyright (C) 2022 , Dmitriy Korovkin
 *
 * This software is licensed as described in the file LICENSE, which
 * you should have received as part of this distribution.
 *
 * You may opt to use, copy, modify, merge, publish, distribute and/or sell
 * copies of the Software, and permit persons to whom the Software is
 * furnished to do so, under the terms of the LICENSE file.
 *
 * This software is distributed on an "AS IS" basis, WITHOUT WARRANTY OF ANY
 * KIND, either express or implied.
 *
 * @file smartbulb
 * 
 ***************************************************************************/
#pragma once

#include <bulb.h>
#include <ctype.h>
#include <iostream>

using namespace std;
class SmartBulb : public Bulb
    {
    public:
    SmartBulb(const string name = "", const string ipAddr = "",
        const string macAddr = "0");
    ~SmartBulb();
    friend ostream& operator<<(ostream& os, const SmartBulb& bulb);
    uint64_t getDeviceMac(void) const
        {
        return m_devMac;
        }
    const string& getDeviceName(void) const
        {
        return m_name;
        }

    const string toString(void) const;

    protected:
    string m_name;     // device name
    uint64_t m_devMac; // device MAC address
    };

