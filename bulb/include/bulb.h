#pragma once

#include <iostream>
#include <map>
#include "udp.h"
#include <jansson.h>

class Bulb
{

public:
    Bulb();
    ~Bulb();

    void setDeviceIP(const std::string& ip);
    std::string getDeviceIp() const;

    /*Get APIs*/
    std::string discover(const std::string& ip);
    std::string getStatus();
    std::string getDeviceInfo();
    std::string getWifiConfig();
    std::string getSystemConfig();
    std::string getUserConfig();

    /*Set APIs*/
    std::string reboot();
    std::string toggleLight(bool state);
    std::string setBrightness(ushort brightness);
    std::string setRGBColor(ushort r, ushort g, ushort b);
    std::string setSpeed(int speed);
    std::string setColorTemp(int temp);
    std::string setScene(ushort scene);

private:
    std::string parseResponse(std::string, std::string addlParams = "");
    
    std::string m_devIP;
    std::map<std::string, std::string> m_paramMap;

    static u_int16_t m_port;
    static UDPSocket m_sock;

protected:
    std::string json_dumps(const json_t *json, size_t flags);
    std::string sendBulbCmd(const std::string& msg);
};

extern const std::string OK_RESULT;
extern const std::string NO_IP_ADDRESS;
extern const std::string ERROR_INVALID_REQUEST;
extern const std::string REPLY_PARSING_ERROR;
extern const std::string UDP_ERROR;
extern const std::string MESSAGE_PARSING_ERROR;
extern const std::string NO_COMMAND_ERROR;
