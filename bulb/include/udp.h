#pragma once

#include <iostream>

class UDPSocket
{

public:
    UDPSocket();
    ~UDPSocket();
    int sendUDPCommand(const std::string& msg,
        std::string& reply,
        const std::string& targetIp,
        const u_int16_t port, std::string& broadcastIP);

    int recvUDPPacket(const u_int16_t port, std::string& reply,
        std::string& broadcastIP);

private:
    bool initializeUDPSocket();
    int sendUDPPacket(const std::string& msg,
        const std::string& targetIp, const u_int16_t port);

    int m_bCastSock;
};
